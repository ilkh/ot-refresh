import random
import requests

from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException
from time import sleep
import logging


def check_block():
    driver.get("https://www.onlinetrade.ru/?city=2")
    try:
        continue_button = driver.find_element_by_xpath('//*[@id="otv3_submit"]')
        logging.warning("Block detected")
        wait_random(5, 12)
        continue_button.click()
        logging.info("Bypassing block")
    except NoSuchElementException:
        logging.info("Looks not blocking")


def urls_to_tabs(URL_LIST):
    driver.get(URL_LIST[0])
    for URL in URL_LIST[1:]:
        driver.execute_script(f'window.open("{URL}");')
    tab_list = driver.window_handles
    return tab_list


def buy_button_notification():
    url = driver.current_url
    logging.info("Checking " + url)
    button = driver.find_element_by_css_selector(".catalog__displayedItem__buttonCover")
    if "Купить" in button.get_attribute('innerText'):
        logging.warning("Buy!")
        requests.post('https://api.telegram.org/bot<token>/sendMessage',
                      data={'chat_id': '<id>', 'text': "Buy! " + url})
        buy = True
    if "Сообщить" in button.get_attribute('innerText'):
        logging.info("Not available")
        buy = False
    return buy


def wait_random(num_from, num_to):
    seconds = random.randrange(num_from, num_to, 2)
    logging.info("Sleeping for " + str(seconds) + " seconds")
    sleep(seconds)


logging.basicConfig(format='%(asctime)s %(levelname)s %(message)s', level=logging.INFO)

URL_LIST = [
    # oem
    "https://www.onlinetrade.ru/catalogue/protsessory-c342/amd/protsessor_amd_ryzen_9_5900x_am4_oem_100_000000061-2369107.html?city=2",
    # box
    "https://www.onlinetrade.ru/catalogue/protsessory-c342/amd/protsessor_amd_ryzen_9_5900x_am4_box_100_100000061wof-2369111.html?city=2"

]


with webdriver.Firefox() as driver:
    driver.implicitly_wait(60)

    check_block()
    buy = False
    tabs = urls_to_tabs(URL_LIST)
    while not buy:
        for tab in tabs:
            driver.switch_to.window(tab)
            if buy_button_notification():
                break
        wait_random(100, 300)
